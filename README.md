# adw-gtk-theme

LibAdwaita Theme for all GTK3 and GTK4 Apps. NOTE: This is a meta package which uses adw-gtk3 for GTK3 and official LibAdwaita theme for GTK4

<br><br>

How to clone this repository:
```
git clone https://gitlab.com/azul4/content/gnome/gradience/adw-gtk-theme.git
```

<br><br>

Requires:

adw-gtk3


